'use strict';
module.exports = (sequelize, DataTypes) => {
  const Intiative = sequelize.define('intiative', {
    initiative_name: DataTypes.STRING,
    ini_certificate_name: DataTypes.STRING,
    local_name: DataTypes.STRING,
    website: DataTypes.STRING,
    sender_email_title: DataTypes.STRING,
    sender_certificate_email_title: DataTypes.STRING,
    request_for_funds: DataTypes.STRING,
    activation_email: DataTypes.STRING,
    ini_counter_type: DataTypes.STRING,
    cert_type: DataTypes.STRING,
    social_text: DataTypes.STRING,
    company_text: DataTypes.STRING,
    user_text: DataTypes.STRING,
    certificate1: DataTypes.STRING,
    certificate2: DataTypes.STRING,
    certificate3: DataTypes.STRING,
    certificate4: DataTypes.STRING,
    certificate_layout: DataTypes.STRING,
    bk_image: DataTypes.STRING,
    user_id: DataTypes.NUMBER,
    mail1_sub: DataTypes.STRING,
    mail1_body: DataTypes.TEXT,
    mail2_sub: DataTypes.STRING,
    mail2_body: DataTypes.TEXT,
    mail3_sub: DataTypes.STRING,
    mail3_body: DataTypes.TEXT,
    mail4_sub: DataTypes.STRING,
    mail4_body: DataTypes.TEXT,
    mail5_sub: DataTypes.STRING,
    mail5_body: DataTypes.TEXT,
    mail6_sub: DataTypes.STRING,
    mail6_body: DataTypes.TEXT,
    mail7_sub: DataTypes.STRING,
    mail7_body: DataTypes.TEXT,
    mail8_sub: DataTypes.STRING,
    mail8_body: DataTypes.TEXT,
  }, {
    indexes: [ { unique: true, fields: [ 'initiative_name' ] } ]
  });
  Intiative.associate = function(models) {
    // associations can be defined here
  };
  return Intiative;
};