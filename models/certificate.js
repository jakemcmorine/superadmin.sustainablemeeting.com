'use strict';
module.exports = (sequelize, DataTypes) => {
  const certificate = sequelize.define('certificate', {
    unq_id: DataTypes.STRING,
    client_id: DataTypes.NUMBER,
    oraganiser_id: DataTypes.NUMBER,
    meeting_id: DataTypes.STRING,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    number_of_tree_planted:DataTypes.INTEGER,
    certificate_date:DataTypes.DATE,
    certficate_name:DataTypes.STRING,
    status: DataTypes.STRING
  }, {});
  certificate.associate = function(models) {
    // associations can be defined here
  };
  return certificate;
};