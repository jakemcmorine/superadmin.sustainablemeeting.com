'use strict';
module.exports = (sequelize, DataTypes) => {
  const meetings = sequelize.define('meetings', {
    outlook_meeting_id: DataTypes.STRING,
    subject: DataTypes.STRING,
    location: DataTypes.STRING,
    type: DataTypes.STRING,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    TimeZoneOffset: DataTypes.STRING,
    TimeZoneName: DataTypes.STRING,
    cancellation_date: DataTypes.DATE,
    user_id: DataTypes.NUMBER
  }, {});
  meetings.associate = function(models) {
    // associations can be defined here
    meetings.hasMany(models.meeting_invites,{foreignKey: 'meeting_id' })
  };
  return meetings;
};