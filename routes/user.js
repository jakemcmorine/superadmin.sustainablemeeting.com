const express 			= require('express');
const multer            = require('multer');
const multers3          = require('multer-s3');
const router 			= express.Router();
const userController    = require("../controller/userControll");
const S3 = require('../services/S3');
const config = require('../config/node_env');


const upload = multer({
    storage: multers3({
        s3: S3,
        bucket: config.S3_BUCKET_NAME,
        acl: 'public-read',
        key: function (req, file, cb) {
            let new_file = `bk_images/${Date.now().toString()}-${file.originalname}`;
            cb(null, new_file);
        }
    })
});

router.get("/",userController.userLogin);
router.get("/addUser",userController.addUser);
router.get("/addIntiative",userController.addIntiative);
router.get("/addUserList",userController.addUserList);

router.get("/intiatives", userController.getAllInitiatives);
router.post("/intiative", upload.single('bk_image'), userController.saveIntiative);

router.get("/intiativeList", userController.intiativeList);
router.get("/userList",userController.userList);
router.get("/editUser",userController.editUser);
router.get("/editIntiative/:initiativeId", userController.editIntiative);
router.delete("/intiative/:initiativeId", userController.deleteInitiative);
router.put("/intiative/:initiativeId", upload.single('bk_image'), userController.updateInitiative);
router.get("/intiative/:initiativeId", userController.getInitiative);
router.post("/intiative/:initiativeId/copy", userController.copyInitiative);


// router.get("/forgotPassword",userController.userforgotpassword);
// router.get("/usersetup",userController.usersetup);
// router.get("/invitationlUsers",userController.invitationlUsers);
// router.get("/certificate/download/:img",userController.downloadCertificate); 
// router.get("/certificate/:unqId/:email",userController.getCertificate); 
// router.get("/certificateScreenshot",userController.generateCertificate);
// router.get("/change/psd/:unqId",userController.changePsd); 
// router.get("/setNewpassword/:unqId",userController.setPassword); 

// router.get("/invoicehistory",userController.invoicehistory);
// router.get("/login",userController.userLogin);
// //router.get("/usermeeting",userController.usermeeting);
// router.get("/companyinfo",userController.companyinfo);
// router.get("/downloadmeeting",userController.downloadmeeting);
// router.get("/usersetup",userController.downloadmeeting);




//router.get("/userSignup",userController.userSignup);
//router.post("/userSignUp",userController.postUserSignUp);
//router.get("/verifyUser",userController.verifyUser);
//router.get("/doLogout",userController.logoutUser);
//router.get("/forgotPassword",userController.forgotPassword);

module.exports = router;