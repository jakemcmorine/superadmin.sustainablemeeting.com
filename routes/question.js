const express 			= require('express');
const router 			= express.Router();
const testController = require("../controller/testController");
const middleware = require('../services/middleware');

router.get("/addQuestions",middleware.checkjwtToken,testController.addQuestions)


module.exports = router;