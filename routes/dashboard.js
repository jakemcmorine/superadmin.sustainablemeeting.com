const express 			= require('express');
const router 			= express.Router();
const dashboardController = require("../controller/dashboardController");
const middleware = require('../services/middleware');

router.get("/dashboard",middleware.checkjwtToken,dashboardController.getDashboard)
router.get("/addAdmin",middleware.checkjwtToken,dashboardController.addAdmins)
router.post("/addAdmin",middleware.checkjwtToken,dashboardController.createAdmin)


module.exports = router;