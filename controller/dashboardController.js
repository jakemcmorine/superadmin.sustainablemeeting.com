const axios = require('axios');
module.exports.getDashboard = async (req, res) => {
    
    let adminList = [];
    let responseData = await axios.get(`${CONFIG.AUTH_URL}getadmins`);
                
                responseData.data.data.forEach(function(adminDetails){
                    adminList.push(adminDetails)
                })
    res.render('dashboard/dashboard.ejs', { adminList: adminList });
};

module.exports.addAdmins = async (req, res) => {
    let userLogged = {
        "photo": "",
        "firstName": "ash",
        "email": "ash@yopmail.com"
    }
    res.render('admin/addAdmin.ejs', { userLogged: userLogged });
};

module.exports.createAdmin = async (req, res) => {
    try {
        console.log(req.body);
        let activeStatus = (req.body.status == "Active" ? 1 : 0);
        axios.post(`${CONFIG.AUTH_URL}userSignUp`, {
            "name": req.body.name,
            "password": "Ash@7989",
            "email": req.body.email,
            "userRole": "Admin",
            "phonenumber": req.body.phone_number,
            "address": "",
            "added_by": "",
            "contractor_id": -0,
            "pqms_id": -0,
            "is_active": activeStatus,
            "is_deleted": 0,
            "is_confirmed": 0
        })
            .then(function (response) {
                console.log("data",response.data)
                if(response && response.data)
                {
                    //res.redirect("/dashboard")
                    res.send({"status":200,"message":"user Created Successfully","data":response.data})
                }
            })
            .catch(function (error) {
                console.log("error", error)
                res.send({"status":401,"message":"user Creation Error","data":""})
            })
    } catch (err) {
        console.log(err);
        throw err;
    }
};