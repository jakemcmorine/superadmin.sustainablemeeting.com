const initiativeRepository = require("../repositories/intiativeRepository")
const fileService = require('../services/fileService');
const _ = require("lodash");

module.exports.addUser = async(req,res)=>{
   
      res.render('addInitiativePartner.ejs'); 
};

module.exports.addUserList = async(req,res)=>{
   
    res.render('addInitiativePartnerList.ejs'); 
};

module.exports.saveIntiative = async(req,res)=>{
    try {
        let data = JSON.parse(req.body.data);
        data.initiative_name = data.initiative_name.toLowerCase();
        if (req.file) data.bk_image = req.file.key;
        let initiative = await initiativeRepository.saveIntiative(data);
        res.status(200).json({message: 'initiative saved successfully'});
    } catch (error) {
        if(req.file) {
            try {
                let data = await fileService.deleteFile(req.file.key);
                console.log(`delete file ${req.file.key}`);
            } catch (error) {
                console.error(error, error.stack);
            }
        }
        console.error(error);
        res.status(400).json({'message': 'Initiative Name already registered'});
    }
    // return res.render('addInitiative.ejs')
};

module.exports.copyInitiative = async(req, res) => {
    let initiative = await initiativeRepository.findInitiative(req.params.initiativeId, {raw: true});
    initiative.initiative_name = initiative.initiative_name + " copy";

    // create a copy of certificate image in s3
    let source = initiative.bk_image;
    let fileName = source.substring(24, source.length);
    let destination = `bk_images/${Date.now().toString()}-${fileName}`;
    try {
        let copyData = await fileService.copyFile(source, destination);
        initiative.bk_image = destination;
        console.log(`Successfully copied file from ${source} to ${destination}`);
    } catch(err) {
        console.error(`Error copying file ${source} to ${destination}`);
        console.error(err.stack);
        return res.status(500).json({message: `error copying file from ${source} to ${destination}`});
    }

    // remove unwanted properties that may conflict with the first row.
    delete initiative.id;
    delete initiative.createdAt;
    delete initiative.updatedAt;
    
    console.log(initiative);
    
    let copyInitiative = await initiativeRepository.saveIntiative(initiative);
    return res.status(200).json({
        message: 'successfully copied',
        initiative: copyInitiative
    });
}

module.exports.deleteInitiative = async(req, res) => {
    try {
        let initiativeId = req.params.initiativeId;
        console.log(`initiative id to be deleted ${initiativeId}`);
        initiativeRepository.deleteInitiative(initiativeId)
        .then(() => {
            res.json({ message: "Initiative deleted successfully" }, 200);
        })
        .catch(err => {
            res.status(404).json({message: 'initiative not found'});
        });
    } catch (error) {
        res.status(400).json(err);
    }
}

module.exports.getInitiative = async(req, res) => {
    let initiativeId = req.params.initiativeId;
    let initiative = await initiativeRepository.findInitiative(req.params.initiativeId);
    if(initiative)
        res.status(200).json(initiative);
    else
        res.status(400).json( { message: `Initiative does not exist with id: ${initiativeId}`});
}

module.exports.updateInitiative = async(req, res) => {
    let initiativeId = req.params.initiativeId;
    let initiative = await initiativeRepository.findInitiative(initiativeId, {raw: true});
    let status = initiative ? 200 : 404;

    if(status == 404) {
        if (req.file) {
            try {
                let data = await fileService.deleteFile(req.file.key);
                console.log(`delete file ${req.file.key}`);
            } catch (error) {
                console.error(error, error.stack);
            }
        }
        return res.status(status).json({message: `No initiative found with id: ${initiativeId}`});
    }
    
    if (req.file) {
        try {
            let data = await fileService.deleteFile(initiative.bk_image);
            console.log(`delete file ${initiative.bk_image}`);
            initiative.bk_image = req.file.key;
            console.info(`New file Key: ${req.file.key}`);
        } catch (error) {
            console.error(error, error.stack);
        }
    }
    _.extend(initiative, JSON.parse(req.body.data));
    let updatedInitiatives = await initiativeRepository.updateInitiative(initiativeId, initiative);
    if (updatedInitiatives[0] == 1) {
        res.status(status).json({message: 'initiative updated successfully'});
    }
}

module.exports.getAllInitiatives = async(req, res) => {
    let initiatives = await initiativeRepository.findAllInitiatives();
    return res.status(200).json({initiative_list: initiatives});
}

module.exports.addIntiative = async(req,res) => {
    res.render('addInitiative.ejs');
};
module.exports.intiativeList = async(req,res)=>{
    res.render('initiativeList.ejs');
};
module.exports.userList = async(req,res)=>{
   
    res.render('userlist.ejs');
};
module.exports.userLogin = async(req,res)=>{
   
    res.render('login.ejs');
};
module.exports.editIntiative = async(req,res)=>{   
    res.render('editInitiative.ejs', {initiativeId: req.params.initiativeId});
};
module.exports.editUser = async(req,res)=>{
   
    res.render('addInitiativePartner.ejs');
};

 


