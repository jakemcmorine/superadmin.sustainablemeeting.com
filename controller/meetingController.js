const request = require('request');
var CONFIG =  require('../config/config');
const axios = require('axios');
const path = require('path');
const meetingRepository=require("../repositories/meetingRepository")

module.exports.meetingListPage=async(req,res) =>{
    console.log("request"+req.userId)
    res.render('downloadSustainableMeeting.ejs');
}
module.exports.usermeeting = async(req,res)=>{
   
  res.render('downloadSustainableMeeting.ejs');
}; 
module.exports.meetingCount = async(req,res)=>{
  let mettings          = await meetingRepository.meetingCount({where:{org_id:req.orgId}})
  let meetingInvite     = await meetingRepository.meetingInviteCount({org_id:req.orgId})
  return res.json({meetingCount:mettings,meetingInviteCount:meetingInvite});
  //res.render('downloadSustainableMeeting.ejs');
};
module.exports.meetingCountUser = async(req,res)=>{
  let mettings          = await meetingRepository.meetingCount({where:{user_id:req.userId}})
  let meetingInvite     = await meetingRepository.meetingInviteCount({user_id:req.userId})
  return res.json({meetingCount:mettings,meetingInviteCount:meetingInvite});
  //res.render('downloadSustainableMeeting.ejs');
};
module.exports.meetingList=async(req,res) =>{
  console.log(req.userId)
   let lists = await meetingRepository.getMeetingInfos({where:{user_id:req.userId}})
   
   res.json({data:lists});
  // return lists;
}

