'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Intiatives', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      initiative_name: {
        type: Sequelize.STRING
      },
      ini_certificate_name: {
        type: Sequelize.STRING
      },
      local_name: {
        type: Sequelize.STRING
      },
      website: {
        type: Sequelize.STRING
      },
      sender_email_title: {
        type: Sequelize.STRING
      },
      sender_certificate_email_title: {
        type: Sequelize.STRING
      },
      request_for_funds: {
        type: Sequelize.STRING
      },
      activation_email: {
        type: Sequelize.STRING
      },
      ini_counter_type: {
        type: Sequelize.STRING
      },
      cert_type: {
        type: Sequelize.STRING
      },
      social_text: {
        type: Sequelize.STRING
      },
      company_text: {
        type: Sequelize.STRING
      },
      user_text: {
        type: Sequelize.STRING
      },
      certificate1: {
        type: Sequelize.STRING
      },
      certificate2: {
        type: Sequelize.STRING
      },
      certificate3: {
        type: Sequelize.STRING
      },
      certificate4: {
        type: Sequelize.STRING
      },
      certificate_layout: {
        type: Sequelize.STRING
      },
      bk_image: {
        type: Sequelize.STRING
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Intiatives');
  }
};