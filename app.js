require('./config/node_env');     //instantiate configuration variables

require('ejs');

const express = require('express');

const redisModels = require("./redis");

const path = require('path');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const url = require('url');
const cookieParser = require('cookie-parser');

global.appRoot = path.resolve(__dirname);

var flash = require('express-flash');
const app = express();
const { db } = require('./mysql/models');
/*-- CORS --*/

app.use(cookieParser());

app.use(function (req, res, next) {
    res.locals.priv_key = 'common';
    next();
});


app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization, Content-Type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    res.setHeader('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
});

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//global.appRoot = path.resolve(__dirname);

app.use(expressValidator());


app.use(express.static(__dirname + 'public'));
app.use(express.static('public'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());
/*-- Begin: auth Check Using Passport --*/
app.use(cookieParser(CONFIG.secret));
//app.use(session({ secret: 'max', saveUninitialized: false, resave: false }));
// app.use(session({
//     cookie: { maxAge: 60000000 /* 60000 */ },
//     store: new RedisStore({ host: CONFIG.redis_host, port: CONFIG.redis_port, ttl: 3600 })
//     , secret: CONFIG.secret
//     , resave: true
//     , saveUninitialized: true
// })
// );
//app.use(passport.initialize());
//app.use(passport.session());
//var flash = require('express-flash-messages')


//app.use(flash());


// app.use(function(req, res, next){
//     res.locals.sessionFlash = req.session.sessionFlash;
//     delete req.session.sessionFlash;
//     next();
// });




// app.all('/express-flash', function( req, res ) {
//     req.flash('success', 'This is a flash message using the express-flash module.');
//     res.redirect(301, '/');
// });
/*-- End: auth Check Using Passport --*/


// async function privilege (req, res, next){
//     // console.log(req.user); next();
//    var return_value = await pr.getAuth(req, res, next);
//    console.log(return_value);
//   if(return_value == 1 || return_value == undefined)  next();
//  else res.redirect('/noaccess');

// };

const user = require('./routes/user');
const dashboard = require('./routes/dashboard')
const meetings = require('./routes/meeting')
//const setup=require('./routes/meetingtime')
//  app.use(async(req, res, next) => {
//     if(!req.user) {
//         res.redirect('/');
//     }else {
//         res.locals.userLogged = req.user;  
//         next();
//     }

// });  
app.use('/', user);
//app.use('/', question)
app.use('/', dashboard)
app.use('/meetings',meetings)
//app.use('/setup',setup)

app.use((err, req, res, next) => {
    console.log('Internal Error Received:', err);
    if (!req.url.includes('favicon.ico')) {
        // res.redirect('/internalServerError');
    }

});

app.use((req, res, next) => {
    if (!req.url.includes('favicon.ico')) {
        //res.redirect('/pageNotFound');
    }
});


module.exports = app;