const aws  = require('aws-sdk');
const config = require('../config/node_env');

aws.config.update({
    region: config.AWS_REGION,
    accessKeyId: config.AWS_ACCESSKEYID,
    secretAccessKey: config.AWS_ACCESSKEY
});

module.exports = new aws.S3();