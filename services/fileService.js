const S3 = require('./S3');
const config = require('../config/node_env');

exports.deleteFile = async(key) => {
    S3.deleteObject({
        Bucket: config.S3_BUCKET_NAME,
        Key: key
    }, (err, data) => {
        if(err)
            throw err;
        else return data;
    });
}

exports.copyFile = async(source, destination) => {
    S3.copyObject({
        Bucket: config.S3_BUCKET_NAME,
        CopySource: source,
        Key: destination
    }, (err, data) => {
        if (err)
            throw err;
        else return data;
    });
}
