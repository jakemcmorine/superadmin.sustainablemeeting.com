Vue.use(VeeValidate);
const app = new Vue({
	el: '#editIntiative',
	data: {
		datas: [],
		intiative: {},
		imagePreview: ''
	},


	methods: {
		// handleFileUpload() {
		// 	this.imageData = this.$refs.file.files[0];
		// 	this.imagePreview = URL.createObjectURL(imageData);
		// },

		UpdateInitiativeData () {


			this.$validator.validate().then(valid => {

				if (valid) {


					this.intiative["mail1_body"] = CKEDITOR.instances.mail1_body.getData();
					this.intiative["mail2_body"] = CKEDITOR.instances.mail2_body.getData();
					this.intiative["mail3_body"] = CKEDITOR.instances.mail3_body.getData();
					this.intiative["mail4_body"] = CKEDITOR.instances.mail4_body.getData();
					this.intiative["mail5_body"] = CKEDITOR.instances.mail5_body.getData();
					this.intiative["mail6_body"] = CKEDITOR.instances.mail6_body.getData();
					this.intiative["mail7_body"] = CKEDITOR.instances.mail7_body.getData();
					this.intiative["mail8_body"] = CKEDITOR.instances.mail8_body.getData();

					this.submitInitform();


				} else {

					$('#formValidErrMsg').modal('show');
					
					console.log(this.$validator)
					// alert("false")
				}
			});

			
					
			
		},

		submitInitform(){

			var form_data = new FormData();
			form_data.append('bk_image', this.imageData);
			form_data.append('data', JSON.stringify(this.intiative));
			const options = {
				headers: {
					"Authorization": document.cookie
				}
			};
			// this.ajaxSetup();
			axios.put("/intiative/"+window.location.pathname.split("/").pop(),
					form_data, options)
				.then(response => {
					this.success = true;
					$('#RecordUPdated').modal('show');
					this.getInitiativesRecord();

					setTimeout(function () {
						window.location.href = window.location.origin + "/intiativeList";
					}, 3000)

					this.timeout();

				})
				.catch(error => {

					if(error.response.status == 400){
						$('#addInitiativewithSameNameAlert').modal('show');
					}


				})
			
		},
		getInitiativesRecord(){
			// this.alertMsg = "loaded";
			this.ajaxSetup();
			$.get("/intiative/"+window.location.pathname.split("/").pop(),
					this.intiative, {
						withCredentials: true,
						crossDomain: true
					})
				.then(response => {
					CKEDITOR.instances.mail1_body.setData(response.mail1_body);
					CKEDITOR.instances.mail2_body.setData(response.mail2_body);
					CKEDITOR.instances.mail3_body.setData(response.mail3_body);
					CKEDITOR.instances.mail4_body.setData(response.mail4_body);
					CKEDITOR.instances.mail5_body.setData(response.mail5_body);
					CKEDITOR.instances.mail6_body.setData(response.mail6_body);
					CKEDITOR.instances.mail7_body.setData(response.mail7_body);
					CKEDITOR.instances.mail8_body.setData(response.mail8_body);
					this.success = true;
					this.intiative = response;


					

					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})


		},
		handleFileUpload() {
			this.imageData = this.$refs.file.files[0];
			this.imagePreview1 = URL.createObjectURL(this.imageData);
		},
		previewFiles(event) {
			this.imageData = event.target.files[0]
		},
		getAll() {

		},
		deleteUSer(id) {
			this.id = id;
		},
		ajaxSetup() {
			$.ajaxSetup({
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", document.cookie);
					xhr.setRequestHeader('Access-Control-Allow-origin', 'true');
				}
			});
		},
		handleSubmit(e) {
			alert("submitting");
			this.submitted = true;

			CKEDITOR.instances.mail1_body.updateElement();
			CKEDITOR.instances.mail2_body.updateElement();
			CKEDITOR.instances.mail3_body.updateElement();
			CKEDITOR.instances.mail4_body.updateElement();
			CKEDITOR.instances.mail5_body.updateElement();
			CKEDITOR.instances.mail6_body.updateElement();
			CKEDITOR.instances.mail7_body.updateElement();
			CKEDITOR.instances.mail8_body.updateElement();

			this.$validator.validate().then(valid => {

				if (valid) {


					this.intiative["mail1_body"] = CKEDITOR.instances.mail1_body.getData();
					this.intiative["mail2_body"] = CKEDITOR.instances.mail2_body.getData();
					this.intiative["mail3_body"] = CKEDITOR.instances.mail3_body.getData();
					this.intiative["mail4_body"] = CKEDITOR.instances.mail4_body.getData();
					this.intiative["mail5_body"] = CKEDITOR.instances.mail5_body.getData();
					this.intiative["mail6_body"] = CKEDITOR.instances.mail6_body.getData();
					this.intiative["mail7_body"] = CKEDITOR.instances.mail7_body.getData();
					this.intiative["mail8_body"] = CKEDITOR.instances.mail8_body.getData();

					this.submitInitform();


				} else {
					console.log(this.$validator)
					// alert("false")
				}
			});
		},
		handleSubmiDomain(e) {
			//  alert(e)
			this.submitted = true;
			this.$validator.validateAll("step2").then(valid => {
				if (valid) {
					this.updateSettings({
						"domain_url": this.userSetting.domain_url
					})
				} else {
					// alert("false")
				}
			});
		},
		handleSubmiInfoUrl(e) {
			alert(e)
			this.submitted = true;
			this.$validator.validateAll("step3").then(valid => {
				if (valid) {
					this.updateSettings({
						"info_url": this.userSetting.info_url
					})
				} else {
					// alert("false")
				}
			});
		},
		handleSubmitFooter(e) {
			var desc = CKEDITOR.instances.footerr.getData();
			this.userSetting.footer = desc;
			this.submitted = true;
			// alert(desc)
			this.$validator.validateAll("step4").then(valid => {
				if (valid) {
					this.updateSettings({
						"footer": this.userSetting.footer
					})
				} else {
					console.log(this.$validator)
					// alert("false")
				}
			});
		},
		shoLoader() {
			$(".loader").css({
				"display": "none"
			});
			var frm = document.getElementsByName('add-manually')[0];
			$("#scss").text("user deleted successfully")
			$("#error").hide();
		},
		hideLoader() {
			$(".loader").css({
				"display": "none"
			});
			$("#success").hide();
			$("#error").show();
		},
		updateSettings(data) {
			this.ajaxSetup();
			$.post("/intiative",
					this.intiative, {
						withCredentials: true,
						crossDomain: true
					})
				.then(response => {
					this.success = true;
					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})
		},

		submitInitform() {



			var form_data = new FormData();
			if(this.imageData){
				form_data.append('bk_image', this.imageData);
			}
			// else{
			// 	form_data.append('bk_image', this.intiative.bk_image);
				
			// }
			// form_data.append('bk_image', this.imageData);
			delete this.intiative.bk_image;
			form_data.append('data', JSON.stringify(this.intiative));
			const options = {
				headers: {
					"Authorization": document.cookie
				}
			};
			// this.ajaxSetup();
			axios.put("/intiative/"+window.location.pathname.split("/").pop(),
					form_data, options)
				.then(response => {
					this.success = true;
					$('#RecordUPdated').modal('show');
					this.getInitiativesRecord();

					setTimeout(function () {
						window.location.href = window.location.origin + "/intiativeList";
					}, 3000)


					this.timeout();

				})
				.catch(error => {})
		},


		timeout() {
			let that = this;
			setTimeout(function () {
				that.success = false
			}, 7000)
		},
		getSettings() {
			this.ajaxSetup();
			$.post("/getUserSetting", {}, {
					withCredentials: true,
					crossDomain: true
				})
				.then(response => {
					if (response.message != null)
						this.userSetting = response.message;
				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})
		}

	}
})


app.getInitiativesRecord()