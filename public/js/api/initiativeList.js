Vue.use(VeeValidate);
const app = new Vue({
	el: '#intiativeList',
	data: {
		datas: [],
		initiativesListItem: [],
		intiative: {},
		deleteMsg: "",
		selectedId: "",
		showToast: false

	},


	methods: {

		confirmDeleteRecord(id) {
			this.selectedId = id;
			this.deleteMsg = "Do you want to delete this record?";
			$('#InitiativeListModal_deleted').modal('show');

		},

		copyRecord(id){
			alert("record copying..");
		},


		ajaxSetup() {
			$.ajaxSetup({
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", document.cookie);
					xhr.setRequestHeader('Access-Control-Allow-origin', 'true');
				}
			});
		},
		getInitiativesList(data) {
			// this.alertMsg = "loaded";
			this.ajaxSetup();
			$.get("/intiatives",
					this.intiative, {
						withCredentials: true,
						crossDomain: true
					})
				.then(response => {
					this.success = true;
					this.initiativesListItem = response.initiative_list;
					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})
		},
		deleteUSer() {
			let id = this.selectedId;
			this.ajaxSetup();
			axios.delete("/intiative/" + id, {
					withCredentials: true,
					crossDomain: true
				})
				.then(response => {
					if (response.status === 200) {

						this.getInitiativesList();
						this.showToast = true;
						$('#InitiativeListModal_deleted').modal('hide');


						setTimeout(() => {
							this.showToast = false
						}, 2000)
					}
					if (response.status === 404) {
						$('#InitiativeListModal_recordenotfound').modal('show');
					}
					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})


		},

		getacopyOfRow(id){
			alert("copying...");
			this.ajaxSetup();
			$.get("/intiative/"+id,
					this.intiative, {
						withCredentials: true,
						crossDomain: true
					})
				.then(response => {
					this.success = true;
					this.datas = response;
					this.copyNewRowData(response);
					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})


		},

		copyNewRowData(data) {

			var form_data = new FormData();
			form_data.append('bk_image', this.imageData);
			form_data.append('data', JSON.stringify(this.intiative));
			const options = {
				headers: {
					"Authorization": document.cookie
				}
			};
			// this.ajaxSetup();
			axios.post("/intiative",
					form_data, options)
				.then(response => {
					this.success = true;
					// alert("form submitted");
					$('#addInitiativeModal').modal('show');
					setTimeout(function () {
						window.location.href = window.location.origin + "/intiativeList";
					}, 3000)


					this.timeout();

				})
				.catch(error => {})
		}


	}
})

app.getInitiativesList()