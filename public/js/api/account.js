var json;
var myJSON;
Vue.use(VeeValidate);

var inviteformstrings;
    const app = new Vue({
      el: '#accountDet',
      data: {
        psdChange:{},
        password: "",
        confirmpassword: "",
        userdet:{
          chnge:{},
          saved:{}
        },
        id:"",
        changPsd:false,
        success:false,
        error:false,
        sccsmsg:"",
        errormsg:"",
        successDet:false,
        errorDet:false,
        sccsmsgDet:"",
        errormsgDet:""
      },
      methods: {
        ajaxSetup(){
            $.ajaxSetup({
              beforeSend: function (xhr)
              {
                 xhr.setRequestHeader("Authorization",document.cookie);        
              }
          });
        },

        changPsdShow(){
           if(this.changPsd){
            this.changPsd=false;
           }else{
            this.changPsd=true;
           }
        },
        psdChangeSubmit(){
            this.$validator.validateAll("changePsd").then(valid => {
                if (valid) {
                    this.changePsd()
                 }else{
                   console.log(this.$validator)
                   // alert("false")
                 }
              });
        },
        saveDetailsSubmit(){
          this.$validator.validateAll("acntDetail").then(valid => {
              if (valid) {
                  this.saveDetail()
               }else{
                 console.log(this.$validator)
                 // alert("false")
               }
            });
      },
      saveDetail(){
        let that=this;
     this.ajaxSetup();
     
    $.post(auth_url+"/v1/saveDetail",
                this.userdet.chnge,{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    that.successDet=true;
                    that.errorDet=false;
                    that.userdet.saved=that.userdet.chnge
                    that.sccsmsgDet=response.message;
           })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
                that.errorDet=true;
                that.successDet=false;
                that.errormsgDet=error.responseJSON.error;
            
              //  alert("some thing went wrong...");
            })
      },
      changePsd(){
        let that=this;
     this.ajaxSetup();
     var pathname = window.location.pathname
     pathname=pathname.split("/")
     console.log(pathname);
    $.post(auth_url+"/v1/changepassword",
                this.psdChange,{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    that.success=true;
                    that.error=false;
                    that.sccsmsg=response.message;
                    var frm = document.getElementsByName('add-manually')[0];
                   $("#scss").text("user deleted successfully")
                    $("#error").hide();
            })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
                that.error=true;
                that.success=false;
                that.errormsg=error.responseJSON.error;
                            //  alert("some thing went wrong...");
            })
      },
 getProfileData(){
      let that=this;
     this.ajaxSetup();
    $.post(auth_url+"/v1/getAccountDet",
                {},{withCredentials: true,crossDomain: true})
                .then(response => {
                    $(".loader").css({"display":"none"});
                    
                    that.userdet.chnge=response.message;
                    that.userdet.saved=response.message
                  
            })
            .catch(error => {
                //$(".loading").hide();
                $(".loader").css({"display":"none"});
              
                that.errormsg=error.responseJSON.error;
                            //  alert("some thing went wrong...");
            })
      },
 
   },
  
   });

   app.getProfileData();
   

