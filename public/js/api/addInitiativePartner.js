Vue.use(VeeValidate);
const app = new Vue({
    el: '#addIntiativePartner',
    data: {
        datas: [],
        intiativepartner: {},
        initiativeList:{}
    },
    methods: {
        addInitiativePartner() {
            this.$validator.validate().then(valid => {
                if (valid) {
                    this.submitInitform();
                } else {
                    $('#formValidErrMsg').modal('show');
                    console.log(this.$validator)
                    // alert("false")
                }
            });
        },
        companyLogo(){
            this.imageData = this.$refs.file.files[0];
        },
        ajaxSetup() {
			$.ajaxSetup({
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", document.cookie);
					xhr.setRequestHeader('Access-Control-Allow-origin', 'true');
				}
			});
		},
        getInitiativesList() {
			this.ajaxSetup();
			$.get("/intiatives",
					this.intiative, {
						withCredentials: true,
						crossDomain: true
					})
				.then(response => {
                    this.success = true;
                    let initiativeListArr = [];
                    for(var i = 0; i< response.initiative_list.length; i++){
                        var obj = {};
                        obj["id"]=response.initiative_list[i].id;
                        obj["name"]=response.initiative_list[i].initiative_name;
                        initiativeListArr.push(obj)
                    }
					this.initiativeList = initiativeListArr;

					this.timeout();

				})
				.catch(error => {
					//$(".loading").hide();

					//  alert("some thing went wrong...");
				})
		},
        submitInitform(){
			let aaa = this.intiativepartner;
			debugger;
            var form_data = new FormData();
			form_data.append('companyLogo', this.imageData);
            form_data.append('data', JSON.stringify(this.intiativepartner));
			const options = {
				headers: {
					"Authorization": document.cookie
				}
			};
			// this.ajaxSetup();
			axios.post("/intiative",
					form_data, options)
				.then(response => {
					this.success = true;
					// alert("form submitted");
					$('#addInitiativeModal').modal('show');
					setTimeout(function () {
						window.location.href = window.location.origin + "/intiativeList";
					}, 3000)


					this.timeout();

				})
				.catch(error => {
					if(error.response.status == 400){
						$('#addInitiativewithSameNameAlert').modal('show');
					}
				})
        }
    }
})
app.getInitiativesList();