const app = new Vue({
    el: '#dashboardview',
    data: {
      datas: [],
      treeCount:"",
      meetingCount:"",
      treeplanted:"",
      userSetting: {
       },
      id:"",
      success:false,
    },
    
   
    methods: {
      getAll() {
       
      },
      deleteUSer(id){
          this.id=id;
      },
      ajaxSetup(){
          $.ajaxSetup({
            beforeSend: function (xhr)
            {
               xhr.setRequestHeader("Authorization",document.cookie);        
            }
        });
      },
      handleSubmitEmailSceduler(e) {
          //alert(e)
       
    },
    handleSubmiDomain(e) {
      //  alert(e)
     
  },
    handleSubmiInfoUrl(e) {
     
  },
  handleSubmitFooter(e) {
   
    },
      shoLoader(){
         $(".loader").css({"display":"none"});
         var frm = document.getElementsByName('add-manually')[0];
         $("#scss").text("user deleted successfully")
         $("#error").hide();
      },
      hideLoader(){   
          $(".loader").css({"display":"none"});
          $("#success").hide();
          $("#error").show();
      },
      getMeetingCounts(){
       this.ajaxSetup();
      $.post(meetingUrl,
                  {},{withCredentials: true,crossDomain: true})
                  .then(response => {
                      console.log(response)
                      this.meetingCount=response.meetingCount;
                      this.treeplanted=response.meetingInviteCount[0].meeting_invites_Count;
                    this.success=true;
                    this.timeout();
              })
              .catch(error => {
                  //$(".loading").hide();
               
                //  alert("some thing went wrong...");
              })
        },
        timeout(){
          let that=this;
          setTimeout(function () { that.success=false}, 7000)
        },
        getSettings(){
          this.ajaxSetup();
         $.post(auth_url+"/v1/getUserSetting",
                     {},{withCredentials: true,crossDomain: true})
                     .then(response => {
                     //  console.log(response);
                     if(response.message!=null)
                       this.userSetting=response.message;
                 })
                 .catch(error => {
                     //$(".loading").hide();
                  
                   //  alert("some thing went wrong...");
                 })
           }
   
    }})

    app.getMeetingCounts();
    if(meetingUrl=='/meetings/count')
        app.getSettings();