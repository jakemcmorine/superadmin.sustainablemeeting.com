$(function () {
    $("form[name='login-form']").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 7
            }
        },
        // Specify validation error messages
        messages: {
            email:{
                required:"<h6 class='loginvalidate fieldvalidate fld_vld adm_lg fl_vld'>Please fill out this field</h6>",
                email:"<h6 class='loginvalidate fieldvalidate fld_vld adm_lg fl_vld'>Invalid email address</h6> "
            } ,
            password: {
                required: "<h6 class='loginvalidate fieldvalidate fld_vld adm_lg pw_vld'>Please provide a password</h6>",
                minlength: "<h6 class='loginvalidate fieldvalidate fld_vld adm_lg pw_vld'>Your password must be at least 7 characters long</h6>"
            },
           
        },
        submitHandler: function (form) {
           event.preventDefault();
          //form.sendData();
          let data= $('#loginform').serialize();
          sendData(data);
          //userloginlist(data);
          return false;
        }
        
        });

        // invite users page add manually form
        $("form[name='add-manually']").validate({
            rules: {
                first_name:"required",
                last_name:"required",
                email: {
                    required: true,
                    email: true
                }
            },
            //Specify validation error messages
            messages:{
                first_name:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                last_name:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                email:{
                    required:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                    email:"<h6 class='fieldvalidate ml-3'>Invalid email address</h6>"
                },
            },
            submitHandler: function (form) {
                 // form.submit();
                 inviteUser($('#add_manually').serialize());
               }
        })


        $("form[name='upload_csv']").validate({
            rules: {
                upload:"required"
               
            },
            //Specify validation error messages
            messages:{
                upload :"<h6 class='fieldvalidate ml-3'>Please add a file</h6>"
               
            },
            submitHandler: function (form) {
                  form.submit();
                 //inviteUser($('#add_manually').serialize());
               }
        })

        
        //setup page url sent
        $("form[name='url-form']").validate({
            rules:{
                url:{
                    required: true,
                    url:true
                }
            },
            messages:{
                url:{
                    required:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                    url:"<h6 class='fieldvalidate ml-3'>Invalid url</h6> "
                },
            },
            submitHandler: function (form) {
                form.submit();
              
             }
            
        })

        //meeting attendees to recieve the email with their tree planting
        $("form[name='pass-time']").validate({
            rules:{
                //sendmail:"required",
                "quantity":{
                    required:true,
                    Range: 12,
                }
                ,
                "quantity2":{
                    required:true,
                    Range: 59,
            },
            },
            messages:{
               // sendmail:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                "quantity":{
                    required: "<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                    Range:"<h6 class='fieldvalidate ml-3'>select number 12 or less </h6>"
                },

                "quantity2":{
                    required:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>",
                    Range:"<h6 class='fieldvalidate ml-3'>please select number less than 60</h6>"
            },
            },
            submitHandler: function (form) {
                // form.submit();
                console.log('submit handler');
                sentMail($("form[name='pass-time']").serialize());
              }
        })

        //Change domain 
        $("form[name='change-domain']").validate({
            rules:{
                change:"required"
            },
            messages:{
                change:"<h6 class='fieldvalidate ml-3'>Please fill out this field</h6>"
            },
            submitHandler: function(form){
                form.submit();
            }
        })
});

    
