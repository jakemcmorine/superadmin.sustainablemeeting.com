jQuery(document).ready(function() {
    jQuery('#example').DataTable({
        columnDefs  : [{ orderable: false, targets: 'no-sort' },{searchable:false, targets: 'no-sort'}],
        aaSorting   : [[0,'asc']]
    });

    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery("#btnBack").click(function() {
        window.location.href = '/admin/list';
    });

    jQuery('#example').on('click','#btnSubmit', function() {
        var adminId     = jQuery(this).attr('data-id');
        var adminName   = jQuery(this).attr('data-user');
        var strInput    = `input:checkbox[id^=privilege_${adminId}]:checked`;
        var aPrivilege  = [];
        jQuery(strInput).each(function() {
            aPrivilege.push(jQuery(this).val());
        });
        var formData = {adminId:adminId, aPrivilege:aPrivilege};
        jQuery.confirm({
            title: 'Confirmation',
            content: `Are you sure you want to assign the selected rights to user: ${adminName}`,
            type: 'green',
            buttons: {   
                ok: {
                    text: "Ok",
                    btnClass: 'btn-warning',
                    keys: ['enter'],
                    action: function(){
                        openOverlay();
                        jQuery.post('/admin/manage-rights', formData, function(data) {
                            if(data.success === 1) {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                                jQuery("#alertType").text("Success");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/manage-rights';
                                }, 3000);
                            }else {
                                closeOverlay();
                                jQuery("#alertBlock").removeClass('alert-success').addClass("alert-danger");
                                jQuery("#alertType").text("Failed");
                                jQuery("#alertMsg").text(data.message);
                                jQuery('#alertBlock').show();
                                setTimeout(function() {
                                    window.location.href = '/admin/manage-rights';
                                }, 3000);
                            }
                        }); 
                    }
                },
                cancel: function(){
                    return true;
                }
            }
        });
    });
});