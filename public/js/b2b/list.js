jQuery(document).ready(function() {
    jQuery('#example').DataTable({
        columnDefs  : [{ orderable: false, targets: [0,2,3] },{ searchable: false, targets: [0,3] }],
        aaSorting   : [[1,'asc']]
    });

    jQuery("#btnAdd").click(function() {
        window.location.href = '/b2b/registration';
    });

    jQuery("#alertBlock").hide();
});