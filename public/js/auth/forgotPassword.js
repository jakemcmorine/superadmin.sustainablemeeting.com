jQuery(document).ready(function() {
    jQuery("#alertBlock").hide();

    jQuery("#btnReset").click(function() {
        window.location.reload(true);
    });

    jQuery('#forgotPassword').validate({
        rules: {
            email: {
                required:true,
                email:true
            }
        },
        messages: {
            email: {
                required: "Please Enter E-mail"
            }
        },
        submitHandler: function() {
            openOverlay();
            jQuery('#btnBlock').hide();
            var formData = jQuery('#forgotPassword').serializeArray();
            jQuery.post('/forgotPassword', formData, function(data) {
                if(data.success === 1){
                    jQuery("#alertBlock").removeClass('alert-danger').addClass("alert-success");
                    jQuery("#errMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }else {
                    jQuery('#btnBlock').show();
                    jQuery("#alertBlock").addClass("alert-danger");
                    jQuery("#errMsg").text(data.message);
                    jQuery('#alertBlock').show();
                }
                closeOverlay();
            });
        }
    });
});