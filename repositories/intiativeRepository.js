const fs=require("fs");
const Sequelize=require("sequelize");
const S3 = require('../services/S3');
const config = require('../config/node_env');
const intiativeModel=require("../models").intiative;

class InitiativeRepository {
  async findInitiative(id, options = null) {
     let initiative = await intiativeModel.findByPk(id, options);
     return initiative;
  }

  async findAllInitiatives() {
    let initiatives = await intiativeModel.findAll();
    return initiatives;
  }

  async initiativeCount(where) {
    let initiativeCount=await intiativeModel.count(where);
    return initiativeCount;
  }

  async saveIntiative(body){
    let intiative = await intiativeModel.create(body);
    return intiative;
  }

  async updateInitiative(initiativeId, data) {
    return await intiativeModel.update(
      data,
      {where: { id: initiativeId }}
    );
  }

  async deleteInitiative(id) {
    let initiative = await intiativeModel.findByPk(id);
    S3.deleteObject({
      Bucket: config.S3_BUCKET_NAME,
      Key: initiative.bk_image
    }, (err, data) => {
      if (err)
        console.log(`Error deleting file Key: ${initiative.bk_image}`);
      else
        console.log(`File deleted succefully Key: ${initiative.bk_image}`);
    });
    return await intiativeModel.destroy({where: {id: id}});
  }
}

module.exports = new InitiativeRepository();